<?php
require_once("uploadDAO.php");
$uploadDao = new uploadDAO();
if(isset($_POST['submit'])!=""){
  $name=$_FILES['file']['name'];
  $size=$_FILES['file']['size'];
  $type=$_FILES['file']['type'];
  $temp=$_FILES['file']['tmp_name'];
  $ext_str = "gif,jpg,jpeg,png,xls,exe,doc,ppt,txt,pdf,html,htm,zip";
  $allowed_extensions=explode(',',$ext_str);
  $max_file_size = 10485760;
  $ext = substr($name, strrpos($name, '.') + 1);
  if (!in_array($ext, $allowed_extensions) ) {
    $uploadDao->error =  "Only ".$ext_str." files are allowed to upload";
  } 
  elseif($size>=$max_file_size){
    $uploadDao->error =  "Only file less than ".$max_file_size."mb are allowed to upload";
  }
  if(!$uploadDao->error){
    move_uploaded_file($temp,"upload/".$name);
    $uploadDao->uploadFile($name);
  }
  
}
?>
<html>
<head>
  <title>ShareTo</title>
  <link rel="stylesheet" href="css/style.css" />
</head>
<body>

<h3><p align="center">ShareTo</p></h3>
<h4><p align="center"><?php if (isset($uploadDao->error)){ echo $uploadDao->error;} ?></p></h4>		
<form enctype="multipart/form-data" action="" name="form" method="POST">
<table border="0" cellspacing="0" cellpadding="5" id="table">
<tr>
<th >Upload File</th>
<td ><label for="file"></label>
<input type="file" name="file" id="file" /></td>
</tr>

<tr>
<th colspan="2" scope="row"><input type="submit" name="submit" id="submit" value="Submit" /></th>
</tr>
</table>
</form>
<br />
<br />
<table border="0" align="center" id="table1" cellpadding="5" cellspacing="0">
  <tr>
    <td colspan="3" align="center">List of files</td>
  </tr>
  <?php
    $uploadDao = new uploadDAO();
    foreach($uploadDao->getUploadList() as $row){
      $name=$row['name'];
      $id=$row['id'];
  ?>
  <tr>
    <td width="150">
    <?php echo $name;?><td align="center"><a href="download.php?filename=<?php echo $name;?>">Download</a></td><td align="center"><a href="delete.php?id=<?php echo $row["id"]; ?>&name=<?php echo $row["name"]; ?>">Delete</a></td>
    </td>
  </tr>
  <?php }?>
</table>
</body>
</html>