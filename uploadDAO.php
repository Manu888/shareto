<?php
    require_once("connection.php");
    
    class uploadDAO {
        public $result;
        public $error = null;
        
        public function getUploadList() {
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("select * from upload order by id desc");
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				
				return $this->result = $statement->fetchAll();
			}
			catch(PDOException $e){
				echo($e->getCode());
			}

        }
        public function uploadFile($name) {
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("insert into upload(name)values(?)");
                $statement->bindValue(1,$name);
                $statement->execute();
                header("location:index.php");
				
			}
			catch(PDOException $e){
				echo($e->getCode());
			}

        }
        public function deleteFile($id) {
			try{
				$connection = Connection::getConnection();
				$statement = $connection->prepare("DELETE FROM upload WHERE id =?");
                $statement->bindParam(1, $id);
                $statement->execute();
				
			}
			catch(PDOException $e){
				echo($e->getCode());
			}

		}

    }